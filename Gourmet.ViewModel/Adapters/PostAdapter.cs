﻿using System;
using System.Linq;
using Gourmet.Model.API;
using Gourmet.ViewModel.Items;

namespace Gourmet.ViewModel.Adapters
{
    public class PostAdapter : BaseAdapter<Post, PostViewModel>
    {
        private readonly string _baseUri;

        public PostAdapter(string baseUri)
        {
            if (string.IsNullOrEmpty(baseUri))
                throw new ArgumentOutOfRangeException("baseUri");
            _baseUri = baseUri;
        }

        public override void Init(Post input, PostViewModel output)
        {
            output.Title = input.Title;
            output.PhotoUrls =
                input.PhotoUrls.Select(relativeUrl => _baseUri + "/" + relativeUrl).ToList();
        }
    }
}
