﻿using System.Collections.ObjectModel;
using System.Linq;
using Gourmet.Model.API;
using Gourmet.ViewModel.Items;

namespace Gourmet.ViewModel.Adapters
{
    public class PostsAdapter : BaseAdapter<PostsResponseModel, PostsPageViewModel>
    {
        public override void Init(PostsResponseModel input, PostsPageViewModel output)
        {
            if (input.Posts != null)
            {
                var postAdapter = new PostAdapter(input.ImagesRootUrl);
                output.Posts = new ObservableCollection<PostViewModel>(input.Posts.Select(postAdapter.Convert));
            }
        }
    }
}
