﻿using System.Runtime.Serialization;
using MetroLab.Common;

namespace Gourmet.ViewModel
{
    public class PageViewModel : SimplePageViewModel
    {
        private volatile int _activeLoadingsCount;


        private bool _isLoadingActive;

        [IgnoreDataMember]
        public bool IsLoadingActive
        {
            get { return _isLoadingActive; }
            private set { SetProperty(ref _isLoadingActive, value); }
        }

        public void BeginDisplayLoading()
        {
            if (_activeLoadingsCount < 0)
                _activeLoadingsCount = 0;
            _activeLoadingsCount++;
            IsLoadingActive = _activeLoadingsCount > 0;
        }

        public void EndDisplayLoading()
        {
            _activeLoadingsCount--;
            if (_activeLoadingsCount < 0)
                _activeLoadingsCount = 0;
            IsLoadingActive = _activeLoadingsCount > 0;
        }
    }
}
