﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Gourmet.ViewModel.Adapters;
using Gourmet.ViewModel.Items;

namespace Gourmet.ViewModel
{
    public class PostsPageViewModel : PageViewModel
    {
        private GourmetClient _gourmetClient;

        public GourmetClient GourmetClient
        {
            get { return _gourmetClient; }
            set { SetProperty(ref _gourmetClient, value); }
        }

        private ObservableCollection<PostViewModel> _posts;

        public ObservableCollection<PostViewModel> Posts
        {
            get { return _posts; }
            set { SetProperty(ref _posts, value); }
        }

        public override async Task InitializeAsync()
        {
            var postsModels = await GourmetClient.GetPostsAsync();
            var postsAdapter = new PostsAdapter();
            postsAdapter.Init(postsModels, this);
        }

    }
}
