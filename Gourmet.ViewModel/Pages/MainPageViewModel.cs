﻿using System;
using System.Diagnostics;
using System.Windows.Input;
using MetroLab.Common;

namespace Gourmet.ViewModel
{
    public class MainPageViewModel : PageViewModel
    {
        private GourmetClient _gourmetClient;

        public GourmetClient GourmetClient
        {
            get { return _gourmetClient; }
            set
            {
                if (SetProperty(ref _gourmetClient, value))
                    OnPropertyChanged("IsAuthenticated");
            }
        }

        private bool IsAuthenticated
        {
            get { return GourmetClient != null; }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private ICommand _signInCommand;

        public ICommand SignInCommand
        {
            get { return GetCommand(ref _signInCommand, o => SignInAction()); }
        }

        private async void SignInAction()
        {
            try
            {
                if (string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(Password))
                {
                    await ShowDialogAsync("You did not fill out the required fields.", "Sign In");
                    return;
                }
                if (!Email.Contains("@"))
                {
                    await ShowDialogAsync("Email is wrong", "Sign In");
                    return;
                }
                try
                {
                    BeginDisplayLoading();

                    GourmetClient = await GourmetClient.LoginAsync(Email, Password);
                    await ShowDialogAsync("You have successfully logged in", "Sign In");
                    var nextPageViewModel = new PostsPageViewModel { GourmetClient = GourmetClient };
                    await AppViewModel.Current.NavigateToViewModel(nextPageViewModel);
                }
                catch (HttpUnauthorizedException e)
                {
                    const string message = "Error - Invalid email or password";
                    ShowDialogAsync(message, "Sign In");
                }
                catch (Exception e)
                {
                    if (Debugger.IsAttached)
                        Debugger.Break();
                    ShowDialogAsync("Some error occurred!", "Sign In");
                }
                finally
                {
                    EndDisplayLoading();
                }
            }
            catch (Exception e)
            {
                if (Debugger.IsAttached)
                    Debugger.Break();
                throw;
            }
        }
    }
}
