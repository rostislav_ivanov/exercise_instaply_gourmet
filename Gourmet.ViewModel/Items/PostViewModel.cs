﻿using System.Collections.Generic;
using MetroLab.Common;

namespace Gourmet.ViewModel.Items
{
    public class PostViewModel : BaseViewModel
    {
        private string _title;

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private List<string> _photoUrls;

        public List<string> PhotoUrls
        {
            get { return _photoUrls; }
            set { SetProperty(ref _photoUrls, value); }
        }
    }
}
