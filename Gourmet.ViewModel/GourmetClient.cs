﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Gourmet.Model.API;
using Newtonsoft.Json;

namespace Gourmet.ViewModel
{
    public class GourmetClient
    {
        protected const string LoginUri = "http://devapi.gourmetfeed.com/v1/login";
        protected const string GetPostsUri = "http://devapi.gourmetfeed.com/v1/posts";

        private string _accessToken;

        protected string AccessToken
        {
            get { return _accessToken; }
        }

        protected static async Task<T> GetJsonResponseAndDeserializeAsync<T>(HttpRequestMessage request)
        {
            var httpClient = new HttpClient();

            var response = await httpClient.SendAsync(request);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
                throw new HttpUnauthorizedException();

            response.EnsureSuccessStatusCode();
            using (var responseStream = await response.Content.ReadAsStreamAsync())
            {
                var jsonSerializer = new JsonSerializer();
                using (var sr = new StreamReader(responseStream))
                using (var jsonTextReader = new JsonTextReader(sr))
                {
                    return jsonSerializer.Deserialize<T>(jsonTextReader);
                }
            }
        }

        protected static internal async Task<TOut> SendPostResponseAsync<TIn, TOut>(string requestUri, TIn requestModel)
            where TIn : class, new()
            where TOut : class, new()
        {
            if (requestUri == null) throw new ArgumentNullException("requestUri");
            if (requestModel == null) throw new ArgumentNullException("requestModel");

            var data = JsonConvert.SerializeObject(requestModel);
            var content = new StringContent(data);
            content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(requestUri, UriKind.RelativeOrAbsolute))
            {
                Content = content
            };
            request.Headers.TryAddWithoutValidation("accept", "application/json");
            request.Headers.TryAddWithoutValidation("Content-Type", "application/json");

            return await GetJsonResponseAndDeserializeAsync<TOut>(request);
        }

        protected internal async Task<T> SendGetResponseWhenAuthenticatedAsync<T>(string requestUri)
           where T : class, new()
        {
            if (requestUri == null) throw new ArgumentNullException("requestUri");

            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(requestUri, UriKind.RelativeOrAbsolute));
            request.Headers.TryAddWithoutValidation("accept", "application/json");
            request.Headers.TryAddWithoutValidation("Content-Type", "application/json");
            request.Headers.TryAddWithoutValidation("apiToken", _accessToken);

            return await GetJsonResponseAndDeserializeAsync<T>(request);
        }

        private GourmetClient() { }

        public static async Task<GourmetClient> LoginAsync(string email, string password)
        {
            if (string.IsNullOrEmpty(email)) throw new ArgumentNullException("email");
            if (string.IsNullOrEmpty(password)) throw new ArgumentNullException("password");

            var responseModel = await SendPostResponseAsync<LoginRequestModel, LoginResponseModel>(LoginUri,
                new LoginRequestModel{Email = email, Password = password, IdentificationService = 1});
            return new GourmetClient {_accessToken = responseModel.AccessToken};
        }

        public async Task<PostsResponseModel> GetPostsAsync()
        {
            return await SendGetResponseWhenAuthenticatedAsync<PostsResponseModel>(GetPostsUri);
        }
    }
}
