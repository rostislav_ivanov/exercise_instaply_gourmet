﻿using System;

namespace Gourmet.ViewModel
{
    public class HttpUnauthorizedException : Exception
    {
        public HttpUnauthorizedException() : base("401 Unauthorized") { }
    }
}
