﻿using Newtonsoft.Json;

namespace Gourmet.Model.API
{
    public class LoginResponseModel
    {
        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
        
        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("identificationService")]
        public string IdentificationService { get; set; }
        
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }
}
