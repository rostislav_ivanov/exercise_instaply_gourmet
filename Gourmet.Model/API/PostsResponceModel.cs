﻿using Newtonsoft.Json;

namespace Gourmet.Model.API
{
    public class PostsResponseModel
    {
        [JsonProperty("imagesRootUrl")]
        public string ImagesRootUrl { get; set; }

        [JsonProperty("posts")]
        public Post[] Posts { get; set; }
    }
}
