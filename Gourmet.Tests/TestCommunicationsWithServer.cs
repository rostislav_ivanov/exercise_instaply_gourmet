﻿using System;
using System.Diagnostics;
using Gourmet.ViewModel;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System.Threading.Tasks;

namespace Gourmet.Tests
{
    [TestClass]
    public class TestCommunicationsWithServer
    {
        private const string DefaultAccountLogin = "ivanov.rostislav@gmail.com";
        private const string DefaultAccountPassword = "a12345";

        [TestMethod]
        public async Task TestLogin1()
        {
            var client = await GourmetClient.LoginAsync(DefaultAccountLogin, DefaultAccountPassword);
            Debug.Assert(client != null, "method LoginAsync() returns null");
        }

        [TestMethod]
        public async Task TestLoginWithWrongPassword()
        {
            try
            {
                var client = await GourmetClient.LoginAsync(DefaultAccountLogin, "wrong password");
            }
            catch (HttpUnauthorizedException)
            {
                return;
            }
            Assert.Fail("We could login with wrong password");
        }

        [TestMethod]
        public async Task TestGetPosts()
        {
            var client = await GourmetClient.LoginAsync(DefaultAccountLogin, DefaultAccountPassword);
            var posts = await client.GetPostsAsync();
            Debug.Assert(posts != null);
            Debug.Assert(posts.ImagesRootUrl != null);
        }
    }
}
